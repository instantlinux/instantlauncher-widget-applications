declare module "desktop-icon-info" {
    export function GetIcon(name: string, size: number): string;
}