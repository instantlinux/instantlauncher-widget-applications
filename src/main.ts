import { exec } from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import * as ini from 'node-ini';
import * as desktop_icon_info from 'desktop-icon-info';
import * as chokidar from 'chokidar';

// Paths to folders containing .desktop-files.
var searchpaths = ["/usr/share/applications"]

interface Application {
    appname: string;
    appicon: HTMLElement;
    appexec: string;
    appcategories: string;
    desktopfile: string;
}

var applications: Array<Application>; // Parsed .desktop-files

var searchIsActive: boolean;

export default function createWidget(widgetfolder: string, closeLauncher: Function) {
    // Main div
    var container = document.createElement("div");
    container.style.position = "absolute";
    container.style.top      = "0";
    container.style.left     = "0";
    container.style.right    = "0";
    container.style.bottom   = "0";
    // Div containing the apps
    var appgrid = document.createElement("div");
    appgrid.style.position  = "absolute";
    appgrid.style.top       = "100px";
    appgrid.style.left      = "5px";
    appgrid.style.right     = "5px";
    appgrid.style.bottom    = "0";
    appgrid.style.overflow  = "auto";
    appgrid.style.textAlign = "center";
    appgrid.id = "appgrid";

    searchIsActive = false;
    applications = [];

    // Custom style for app grid
    var appgridstyle = document.createElement("style");
    appgridstyle.innerHTML = `
    #appgrid::-webkit-scrollbar {
        width: 5px;
    }
    
    #appgrid::-webkit-scrollbar-thumb:vertical {
        margin: 5px;
        background-color: rgba(255, 255, 255, 0.3);
        -webkit-border-radius: 5px;
    }

    #appgrid::-webkit-scrollbar-thumb:vertical:hover {
        background-color: rgba(255, 255, 255, 0.8);
    }
    
    #appgrid::-webkit-scrollbar-button:start:decrement,
    #appgrid::-webkit-scrollbar-button:end:increment {
        height: 5px;
        display: block;
    }

    #appgrid div {
        width: 128px;
        display: inline-block;
        padding: 15px 30px;
        margin: 10px;
        text-align: center;
        cursor: pointer;
        background-color: rgba(0, 0, 0, 0);
        border-radius: 2px;
        transition: 0.2s;
    }
    #appgrid div:hover {
        background-color: rgba(0, 0, 0, 0.4);
        transform: scale(1.1);
        box-shadow: 2px 2px 7px rgba(0, 0, 0, 0.1);
    }
    #appgrid div:active {
        transform: scale(1.3);
    }

    #appgrid p {
        font-family: Raleway;
        color: #fafafa;
        white-space: nowrap;
        width: 100%;
        overflow: hidden;
        padding: 0;
        margin: 20px 0;
    }

    #appgrid img {
        width: 128px;
        height: 128px;
    }`;
    appgrid.appendChild(appgridstyle);

    // Creates an image element for an icon.
    var getIcon = function(iconname: string) {
        let appicon = document.createElement("img");
        appicon.src = "data:image/png;base64," + desktop_icon_info.GetIcon(iconname, 128);

        return appicon;
    }

    var drawicon = function(value: any, desktopfile: string) {
        // Get proper command to exec the application.
        var execcommand = value["Desktop Entry"].Exec;
        var args = ['f', 'F', 'u', 'U', 'd', 'D', 'n', 'N', 'i', 'c', 'k', 'v', 'm'];
        args.forEach(argument => {
            execcommand = execcommand.replace('%' + argument, '');
        });

        var appicon = getIcon(value["Desktop Entry"].Icon);

        // Add the application to the array.
        applications.push({
            appname: value["Desktop Entry"].Name,
            appicon: appicon,
            appexec: execcommand,
            appcategories: value["Desktop Entry"].Categories,
            desktopfile: desktopfile
        });

        // Container for the current item.
        var appdiv = document.createElement("div");

        // App icon.
        appdiv.appendChild(appicon);

        // App name (paragraph).
        var appname = document.createElement("p");
        appname.innerHTML = value["Desktop Entry"].Name;
        appdiv.appendChild(appname);

        // Launch the application.
        appdiv.onclick = function() {
            exec(execcommand, { cwd: process.env["HOME"] || process.env["HOMEPATH"] });
            closeLauncher();
        }

        // Add the item.
        appgrid.appendChild(appdiv);
    };


    // Searchbar
    var searchbar = document.createElement("input");
    // Style
    searchbar.type = "text";
    searchbar.id = "appsearch";
    searchbar.style.position     = "absolute";
    searchbar.style.left         = "50%";
    searchbar.style.top          = "32px";
    searchbar.style.transform    = "translate(-50%, 0)";
    searchbar.style.border       = "0";
    searchbar.style.outline      = "0";
    searchbar.style.padding      = "10px";
    searchbar.style.background   = "none";
    searchbar.style.borderBottom = "1px solid rgba(255, 255, 255, 0.3)";
    searchbar.style.width        = "40%";
    searchbar.style.fontFamily   = "Raleway";
    searchbar.style.fontSize     = "14px";
    searchbar.style.color        = "#fafafa";
    searchbar.style.transition   = "0.2s";
    // Hover transition style.
    var searchbarstyle = document.createElement("style");
    searchbarstyle.innerHTML = `
    #appsearch:hover, #appsearch:focus {
        border-bottom: 1px solid rgba(255, 255, 255, 0.8) !important;
    }`;
    searchbar.appendChild(searchbarstyle);

    // When return is pressed...
    searchbar.onkeyup = function(event) {
        if (event.keyCode == 13) {
            (appgrid.children[1] as any).onclick(); // Launch the first application
        }
    }

    // When the searchbar text changes...
    searchbar.oninput = function() {
        if (searchbar.value == "") {
            searchIsActive = false;
        } else {
            searchIsActive = true;
        }

        while (appgrid.children[1]) { // Remove current items.
            appgrid.removeChild(appgrid.children[1]);
        }
        applications.forEach(application => {
            if (application.appname.toUpperCase().includes(searchbar.value.toUpperCase()) ||
                    application.appexec.toUpperCase().includes(searchbar.value.toUpperCase())) { // Get applications that match the search query
                var execcommand = application.appexec;

                var appdiv = document.createElement("div");
                appdiv.appendChild(application.appicon);
        
                var appname = document.createElement("p");
                appname.innerHTML = application.appname;
                appdiv.appendChild(appname);

                appdiv.onclick = function() {
                    exec(execcommand, { cwd: process.env["HOME"] || process.env["HOMEPATH"] });
                    closeLauncher();
                    searchbar.value = "";
                    (searchbar as any).oninput(null);
                }

                appgrid.appendChild(appdiv); // Add the item to the grid again.
            }
        });
    }

    // Add the searchbar and the appgrid.
    container.appendChild(searchbar);
    container.appendChild(appgrid);

    // Main function
    searchpaths.forEach(_path => {
        fs.readdir(_path, function(err, items) {
            items.forEach(element => {
                if (path.extname((_path + "/" + element).trim()) == ".desktop") {
                    ini.parse(_path + '/' + element, function(err: Error, value: any) {
                        if (!err) {
                            if (value["Desktop Entry"] != undefined && 
                                value["Desktop Entry"].Icon != undefined &&
                                value["Desktop Entry"].Name != undefined &&
                                value["Desktop Entry"].Exec != undefined) {
                                drawicon(value, _path + "/" + element);
                            }
                        }
                    }); // Parse every .desktop file (ini-syntax).
                }
            });
        });
    });

    searchpaths.forEach(_path => {
        let watcher = chokidar.watch(_path, { ignored: /^\./, persistent: true });
        let fileWatcherReady = false;
        watcher
            .on('add', function(filename) {
                if (fileWatcherReady) {
                    if (path.extname(filename.trim()) == ".desktop") {
                        ini.parse(filename, function(err: Error, value: any) {
                            if (!err) {
                                if (value["Desktop Entry"] != undefined && 
                                    value["Desktop Entry"].Icon != undefined &&
                                    value["Desktop Entry"].Name != undefined &&
                                    value["Desktop Entry"].Exec != undefined) {
                                    drawicon(value, filename);
                                    updateGrid(appgrid, closeLauncher);
                                }
                            }
                        }); // Parse every .desktop file (ini-syntax).
                    }
                }
            })
            .on('change', function(filename) {
                if (fileWatcherReady) {
                    applications.forEach(app => {
                        if (app.desktopfile == filename) {
                            if (path.extname(filename.trim()) == ".desktop") {
                                ini.parse(filename, function(err: Error, value: any) {
                                    if (!err) {
                                        if (value["Desktop Entry"] != undefined && 
                                            value["Desktop Entry"].Icon != undefined &&
                                            value["Desktop Entry"].Name != undefined &&
                                            value["Desktop Entry"].Exec != undefined) {
                                            // Get proper command to exec the application.
                                            var execcommand = value["Desktop Entry"].Exec;
                                            var args = ['f', 'F', 'u', 'U', 'd', 'D', 'n', 'N', 'i', 'c', 'k', 'v', 'm'];
                                            args.forEach(argument => {
                                                execcommand = execcommand.replace('%' + argument, '');
                                            });

                                            app.appname = value["Desktop Entry"].Name;
                                            app.appicon = getIcon(value["Desktop Entry"].Icon);
                                            app.appexec = execcommand;
                                            app.appcategories = value["Desktop Entry"].categories;

                                            updateGrid(appgrid, closeLauncher);
                                        } else {
                                            applications.splice(applications.indexOf(app), 1);
                                            updateGrid(appgrid, closeLauncher);
                                        }
                                    }
                                }); // Parse every .desktop file (ini-syntax).
                            }
                        }
                    });
                }
            })
            .on('unlink', function(filename) {
                if (fileWatcherReady) {
                    applications.forEach(app => {
                        if (app.desktopfile == filename) {
                            applications.splice(applications.indexOf(app), 1);
                        }
                    });
                    updateGrid(appgrid, closeLauncher);
                }
            })
            .on('ready', function() {
                fileWatcherReady = true;
                console.log("Ready event!");
            })
    });

    window.addEventListener("keydown", function() {
        searchbar.focus();
    });

    return container;
}

function updateGrid(appgrid: HTMLElement, closeLauncher: Function) {
    if (!searchIsActive) {
        console.log("Updating applications...");
        while (appgrid.children[1]) { // Remove current items.
            appgrid.removeChild(appgrid.children[1]);
        }
        applications.forEach(application => {
                var execcommand = application.appexec;

                var appdiv = document.createElement("div");
                appdiv.appendChild(application.appicon);
        
                var appname = document.createElement("p");
                appname.innerHTML = application.appname;
                appdiv.appendChild(appname);

                appdiv.onclick = function() {
                    exec(execcommand, { cwd: process.env["HOME"] || process.env["HOMEPATH"] });
                    closeLauncher();
                }

                appgrid.appendChild(appdiv); // Add the item to the grid again.
        });
    }
}