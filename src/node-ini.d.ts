declare module "node-ini" {
    export function parse(filename: string, cb: any): any;
}